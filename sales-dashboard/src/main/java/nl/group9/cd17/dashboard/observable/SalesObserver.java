package nl.group9.cd17.dashboard.observable;

import nl.group9.cd17.service.api.domain.Sale;

import java.util.List;

public interface SalesObserver {

    void onSalesUpdate(List<Sale> sale);
}
