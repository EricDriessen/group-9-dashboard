package nl.group9.cd17.service.util;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import nl.group9.cd17.service.api.domain.Sale;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A mapper that maps json to {@link Sale}s.
 */
 public class SaleMapper {
    private static final Type LIST_TYPE = new TypeToken<ArrayList<Sale>>() {}.getType();
    private static Gson gson = new Gson();

    private SaleMapper(){}

    public static Sale jsonToSale(String jsonString) {
        return gson.fromJson(jsonString, Sale.class);
    }

    public static List<Sale> jsonToSales(String entityString) {
        return gson.fromJson(entityString, LIST_TYPE);
    }

    public static String salesToJson(List<Sale> sales) {
        return gson.toJson(sales, LIST_TYPE);
    }

    public static String saleToJson(Sale sale) {
        return gson.toJson(sale);
    }
}
