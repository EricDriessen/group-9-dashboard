package nl.group9.cd17.service.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import nl.group9.cd17.service.datastorage.DataStore;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * AbstractHandler provides a template for a {@link HttpHandler} for the GET and POST methods
 */
abstract class AbstractHandler implements HttpHandler {
    static final int STATUS_OK = 200;
    static final int STATUS_CREATED = 201;

    private Logger logger = Logger.getLogger(this.getClass().getName());
    private String path;

    DataStore dataStore;

    AbstractHandler(String path, DataStore dataStore) {
        this.path = path;
        this.dataStore = dataStore;
    }

    public void handle(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        switch (requestMethod) {
            case "GET":
                this.onGet(exchange);
                break;
            case "POST":
                this.onPost(exchange);
                break;
            default:
                this.logger.log(Level.INFO, "Received a non processable method: " + requestMethod);
                break;
        }
    }

    void sendResponse(HttpExchange exchange, int statusCode, String msg) throws IOException {
        exchange.sendResponseHeaders(statusCode, msg.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(msg.getBytes());
        os.close();
    }

    abstract void onGet(HttpExchange httpExchange) throws IOException;

    abstract void onPost(HttpExchange httpExchange) throws IOException;

    String retrieveMessageBodyAsString(HttpExchange exchange) {
        Scanner s = new Scanner(exchange.getRequestBody()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
