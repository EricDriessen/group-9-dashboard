package nl.group9.cd17.service.handlers;

import com.sun.net.httpserver.HttpExchange;
import nl.group9.cd17.service.api.Paths;
import nl.group9.cd17.service.api.domain.Sale;
import nl.group9.cd17.service.datastorage.DataStore;
import nl.group9.cd17.service.util.SaleMapper;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SaleHandler is a controller for {@link Sale}s.
 */
public class SaleHandler extends AbstractHandler {

    private Logger logger = Logger.getLogger(SaleHandler.class.getName());

    public SaleHandler(DataStore dataStore) {
        super(Paths.SALE_PATH, dataStore);
    }

    void onGet(HttpExchange exchange) throws IOException {
        List<Sale> sales = dataStore.getSales();

        String salesJson = SaleMapper.salesToJson(sales);

        sendResponse(exchange, STATUS_OK, salesJson);
        logger.log(Level.INFO, "onGET: Successfully emitted sales");
    }

    void onPost(HttpExchange exchange) throws IOException {
        String saleJson = retrieveMessageBodyAsString(exchange);
        Sale sale = SaleMapper.jsonToSale(saleJson);

        dataStore.saveSale(sale);
        logger.log(Level.INFO, "onPOST: Successfully saved sale: " + sale.toString());

        sendResponse(exchange, STATUS_CREATED, "");
    }
}