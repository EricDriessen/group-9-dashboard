package nl.group9.cd17.service.api;

/**
 * Config used by the HttpServer and Client.
 */
public class Config {
    
    public static int PORT_NUMBER = 8123;
    
}
