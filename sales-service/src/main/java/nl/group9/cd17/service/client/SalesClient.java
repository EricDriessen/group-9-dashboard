package nl.group9.cd17.service.client;


import nl.group9.cd17.service.api.Config;
import nl.group9.cd17.service.api.Paths;
import nl.group9.cd17.service.api.domain.Sale;
import nl.group9.cd17.service.util.SaleMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple client that can be used to POST a {@link Sale} to the Service, or to GET all sales
 */
public class SalesClient {

    private static final Logger logger = Logger.getGlobal();

    private CloseableHttpClient httpClient;
    private URI salesEndpointUri;

    public SalesClient() throws URISyntaxException {
        this.httpClient = HttpClients.createDefault();
        this.salesEndpointUri = new URI("http://localhost:" + Config.PORT_NUMBER + Paths.SALE_PATH);
    }

    public void postSale(Sale sale) {
        try {
            HttpPost httpPost = new HttpPost(this.salesEndpointUri);

            String saleString = SaleMapper.saleToJson(sale);
            httpPost.setEntity(new StringEntity(saleString));

            CloseableHttpResponse response = this.httpClient.execute(httpPost);
            response.close();

            logger.log(Level.INFO, "POST sale: " + saleString);
        } catch (Exception e) {
            logger.log(Level.INFO, "Couldn't post sales: " + e.getLocalizedMessage());
        }
    }

    public List<Sale> getSales() {
        List<Sale> sales = null;

        HttpGet httpGet = new HttpGet(this.salesEndpointUri);
        try (CloseableHttpResponse response = this.httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            String entityString = EntityUtils.toString(entity);
            sales = SaleMapper.jsonToSales(entityString);

            logger.log(Level.INFO, "GET sales: " + sales);
        } catch (Exception e) {
            logger.log(Level.INFO, "Couldn't retrieve sales: " + e.getLocalizedMessage());
            e.printStackTrace();
        }

        return sales;
    }

}
