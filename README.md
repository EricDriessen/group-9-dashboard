# Java9Dashboard
### Start:
Start eerst ServiceMain, dan DashboardMain, gevolgd door RevenueGeneratorMain.

## Overview

1. **nl.group9.cd17.sales-service** is 'data opslag'. Dit process heeft een http server waarbij het mogelijk
is om 'Sale' objecten te posten of op te vragen.
2. **nl.group9.cd17.revenue-generator**  genereert random sales objecten
 en post deze via een http client naar de service.
3. **nl.group9.cd17.sales-dashboard** is de 'front-end' JavaFX app, en vraagt via een http client met een vast interval de sales objecten op
 bij de server.
