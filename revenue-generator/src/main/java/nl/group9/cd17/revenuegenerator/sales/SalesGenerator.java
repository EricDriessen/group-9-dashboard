package nl.group9.cd17.revenuegenerator.sales;

import nl.group9.cd17.service.api.domain.Sale;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Randomly generates a {@link Sale}
 */
class SalesGenerator {

    private List<String> salesMen = Arrays.asList("Bas", "Rob", "Liesbeth");
    private List<String> consultants = Arrays.asList("Albert", "Alex", "Danny", "David", "Dennis",
            "Eric", "Erik", "Frans", "Henk", "Irmin", "Ivan", "Ivo", "Jelle", "Jeroen", "Joop", "Jurjen", "Marcus",
            "Mark", "Martijn A.", "Martijn v D.", "Morten", "Pascal", "Rinke", "Roel", "Ron v H.", "Ron S.", "Sjors",
            "Talip", "Tiese", "Tom H.", "Tom K.", "Rick");
    private List<String> customers = Arrays.asList("Alphabet", "ASML", "Nasdaq", "Tennet",
            "APG", "Monuta", "Kadaster");
    private Random random = new Random();

    Sale generateSale() {
        return new Sale(
                this.generateSalesMan(),
                this.generateConsultant(),
                this.generateCustomer(),
                this.generateRevenue(),
                LocalTime.now()
        );
    }

    private String generateSalesMan() {
        return this.salesMen.get(random.nextInt(salesMen.size()));
    }

    private String generateConsultant() {
        // Very important business logic, do not touch!!!
        if(random.nextInt(100) >= 97) {
            return "Eric";
        }
        return this.consultants.get(random.nextInt(consultants.size()));
    }

    private String generateCustomer() {
        return this.customers.get(random.nextInt(customers.size()));
    }

    private double generateRevenue() {
        double revenue = random.nextDouble() * 1000;
        revenue = Math.round(revenue);
        return revenue / 100;
    }

}
